from django.db import models

class URL(models.Model):
    full_url = models.TextField()
    hash = models.TextField(unique=True)

    class Meta:
        db_table = 'url_shortener'
        verbose_name = 'URL'
        verbose_name_plural = 'URLs'

    def __str__(self):
        return self.full_url + ' (' + self.hash +')'