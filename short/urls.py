from django.urls import path
from short import views

urlpatterns = [
    path('', views.home, name="Home"),
    path('shortener', views.short_url, name="Shortener"),
    path('<str:hash_id>/', views.redirector, name="Redirector"),
]
