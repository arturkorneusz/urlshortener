from django.shortcuts import redirect
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse, JsonResponse
import string, random 
from .models import URL
from django.views.decorators.csrf import csrf_exempt


def home(request):
    current_site = get_current_site(request)
    return HttpResponse("<h1>Shortener for long URLs</h1><p>To insert new URL use POSTMAN. <br>Link: http://127.0.0.1:8000/shortener <br>Key: 'url' <br>Value: <i>your long url</i></p><p>To check short url - link: http://127.0.0.1:8000/<i>Short hash</i></p>")

def shortit(long_url):
    N = 7
    s = string.ascii_uppercase + string.ascii_lowercase + string.digits
    url_id = ''.join(random.choices(s, k=N))
    URL.objects.create(full_url=long_url, hash=url_id)
    return url_id

@csrf_exempt
def short_url(request):
    long_url = request.POST.get("url")
    hash = shortit(long_url)
    current_site = get_current_site(request)
    data = {
        "success": True,
        "id": hash,
        "link": "http://{}/{}".format(current_site, hash),
        "long_url": long_url
    }
    return JsonResponse(data)

def redirector(request,hash_id=None):
    if URL.objects.filter(hash=hash_id).exists():
        url = URL.objects.get(hash=hash_id)
        return redirect(url.full_url)
    else:
        return JsonResponse({"success":False})
